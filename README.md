# README #

## Cuál es el propósito de este repositorio? ##

* Este es el repositorio con el código de un ejemplo de uso del Salesforce Apex Connector Framework utlizado en esta entrada de blog:
[Entrada del blog con la explicación completa](https://forcegraells.com/2018/03/18/apex-connector-framework/).

    ## Contacto ##

* Para cualquier duda, sugerencia o comentario puedes contactarme en mi [Email Personal](esteve.graells@gmail.com).

Un saludo,
Esteve.
