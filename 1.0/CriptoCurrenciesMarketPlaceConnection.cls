/**
 * Created by 00070129 on 16/03/2018.
 */

global class CriptoCurrenciesMarketPlaceConnection extends DataSource.Connection{

    private final static Boolean WE_ARE_MOCKING_CALLS = true;
    private DataSource.ConnectionParams connectionInfo;

    /**
     * @description Will be invoked by the Provider to send param connections
     *
     * @return nope
     *
     */
    global CriptoCurrenciesMarketPlaceConnection(DataSource.ConnectionParams connectionParams) {
        this.connectionInfo = connectionParams;
    }

    /**
     * @description Override sync method to decalaratively create the external objects schema
     *
     * @return List of tables available to Salesforce Connect as External object candidates
     *
     */
    override global List<DataSource.Table> sync() {

        List<DataSource.Table> tables = new List<DataSource.Table>();

        //Definición de las características del objeto
        DataSource.Table exchangeRatesTable = new DataSource.Table();
        exchangeRatesTable.labelSingular = 'Cripto Currency';
        exchangeRatesTable.labelPlural = 'Cripto Currency';
        exchangeRatesTable.description = 'Criptocurrencies Listing';
        exchangeRatesTable.name = 'Criptocurrency';
        exchangeRatesTable.nameColumn = 'ExternalId';

        //Definición de los campos y relaciones del objeto
        List<DataSource.Column> columns = new List<DataSource.Column>();
        columns = new List<DataSource.Column>();
        columns.add(DataSource.Column.url('DisplayUrl')); //required
        columns.add(DataSource.Column.text('ExternalId', 255)); //required
        columns.add(DataSource.Column.text('Name', 255));
        columns.add(DataSource.Column.text('Symbol', 5));
        columns.add(DataSource.Column.integer('Rank', 3));
        columns.add(DataSource.Column.number('Price USD',10, 5));
        columns.add(DataSource.Column.number('Price EUR',10, 5));
        columns.add(DataSource.Column.number('Percent Change 1h', 4, 2));
        columns.add(DataSource.Column.number('Percent Change 24h', 4, 2 ));
        columns.add(DataSource.Column.text('Last Update', 20));

        //Si quisiéramos declarar una Indirect Lookup sería así:
        //columns.add(DataSource.Column.indirectLookup('Nombre_Campo_Lookup', 'Nombre_Objeto_Destino',                                                                                  'Nombre_Campo_Lookup_Objeto_Destino'));

        exchangeRatesTable.columns = columns;
        tables.add(exchangeRatesTable);

        return tables;
    }


    /**
    * @description Sobreescrtiura del método Query que será invocado cuando el usuario
    * acceda al External Object o se realicen queries via APEX
    *
    * @return TableResult con las filas resultado para que se pasen Salesforce Connect
    *
    */
    override public DataSource.TableResult query(DataSource.QueryContext context){

        //Invocamos al método obtenerFilasResultado que realiza las invocaciones
        //a los sistemas externos y todos el procesamiento asociado (cálculos, etc.)

        return DataSource.TableResult.get(context,
                DataSource.QueryUtils.process(context, obtenerFilasResultado()));
    }


    /**
    * @description Se invoca a las fuentes externas, se crea la estructura tal y como
    * la requiere Salesforce Connect
    *
    * @return un Map que contiene los resultados con las filas, indicando el valor de cada
    * campo resultado
    *
    */
    private List<Map<String,Object>> obtenerFilasResultado() {

        //Obtener la cotización Euro-Dolar
        Decimal EUR_TO_USDDollar = getUSDToEURRate();

        //Obtener la información de las criptomonedas
        List<CriptoCurrencyInfo> criptoMarketList = getCriptoMarketList();

        //Transform data rates in rows to be useful for the odata connector
        List<Map<String,Object>> filas = new List<Map<String,Object>>();
        for (CriptoCurrencyInfo c : criptoMarketList){

            Map<String, Object> filaResultado = new Map<String, Object>();

            filaResultado.put('DisplayUrl', 'callout:Cryptocurrency_Market_Capitalizations');
            filaResultado.put('ExternalId', c.symbol + '-' + c.name);
            filaResultado.put('Name', c.name);
            filaResultado.put('Percent Change 1h', c.percent_change_1h);
            filaResultado.put('Percent Change 24h', c.percent_change_24h);
            filaResultado.put('Price USD', c.price_usd);
            filaResultado.put('Price EUR', c.price_usd * (1/EUR_TO_USDDollar));
            filaResultado.put('Rank', c.rank);
            filaResultado.put('Symbol', c.symbol);

            filas.add(filaResultado);
        }
        return filas;
    }

    /**
    * @description Obtención de la info de las criptomonedas invocación al servicio externo y
    * parseo de la respuesta, dado q la respuesta en un JSON aprovecho el parser de APEX
    *
    * @return listado con la información de las criptomonedas
    *
    */
    private List<CriptoCurrencyInfo> getCriptoMarketList(){
        HttpResponse httpResponse;
        String result='';

        if (!WE_ARE_MOCKING_CALLS) {
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint('callout:Cryptocurrency_Market_Capitalizations/ticker/');
            req.setMethod('GET');
            httpResponse = http.send(req);
            result = httpResponse.getBody();
            System.debug('-ege- result: ' + result);
        }
        else{
            result = MockResponses.MOCKED_RESULT_CRIPTO;
        }

        List<CriptoCurrencyInfo> ccList = new List<CriptoCurrencyInfo>();
        JSONParser parser = JSON.createParser(result);
        while (parser.nextToken() != null){
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                while (parser.nextToken() != null) {
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                        CriptoCurrencyInfo cc =
                                (CriptoCurrencyInfo)parser.readValueAs(CriptoCurrencyInfo.class);
                        ccList.add(cc);
                    }
                }
            }
        }

        return ccList;
    }

    /**
    * @description Obtención de los tipos de cambio relativos al euro mediante
    * invocación del servicio del BCE y parseo de la respuesta
    *
    * @return el tipo de cambio concreto para la pareja euro-dolarUSD
    *
    */
    private Decimal getUSDToEURRate(){
        HttpResponse httpResponse;
        String result='';

        if (!WE_ARE_MOCKING_CALLS) {
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint('callout:EuropeanCentralBank');
            req.setMethod('GET');
            httpResponse = http.send(req);
            result = httpResponse.getBody();
        }
        else{
            result = MockResponses.MOCKED_RESULT_BCE;
        }

        XmlStreamReader xsr = new XmlStreamReader(result);
        Double USDTOEURrate = 0.0;

        while(xsr.hasNext()){
            if (xsr.getEventType() == XmlTag.START_ELEMENT) {

                Integer i = 0;
                while (i < xsr.getAttributeCount()){
                    if (xsr.getAttributeLocalName(i) == 'currency') {
                        if ( xsr.getAttributeValueAt(i) == 'USD') {
                            if (xsr.getAttributeLocalName(i + 1) == 'rate')
                                USDTOEURrate = Double.valueOf(xsr.getAttributeValueAt(i + 1));
                        }
                    }
                    i++;
                }
            }
            xsr.next();
        }
        return USDTOEURrate;
    }
}
