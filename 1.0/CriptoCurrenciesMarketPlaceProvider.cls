
/**
 * Implementación de Apex Connector Framework para la obtención
 * de información de Criptomonedas
 *
 * Descripción completa del artículo en: https://forcegraells.com

 * @author Esteve Graells
 * @date March 2018
 *
 * @description Extiende la clase Provider para indicar las capabilities funcionales
 * de nuestra implementación y las capabilities para la autenticación
 */

global class CriptoCurrenciesMarketPlaceProvider extends DataSource.Provider{

    override global List<DataSource.AuthenticationCapability> getAuthenticationCapabilities()
    {
        List<DataSource.AuthenticationCapability> caps = new List<DataSource.AuthenticationCapability>();
        caps.add(DataSource.AuthenticationCapability.ANONYMOUS);
        return caps;
    }

    override global List<DataSource.Capability> getCapabilities()
    {
        List<DataSource.Capability> caps = new List<DataSource.Capability>();

        caps.add(DataSource.Capability.ROW_QUERY);
        caps.add(DataSource.Capability.SEARCH);

        //Existen más capacidades que en esta implementación no ofrezco
        //capabilities.add(DataSource.Capability.ROW_CREATE);
        //capabilities.add(DataSource.Capability.ROW_UPDATE);
        //capabilities.add(DataSource.Capability.ROW_DELETE);
        return caps;
    }

    override global DataSource.Connection getConnection(DataSource.ConnectionParams connectionParams)
    {
        return (DataSource.Connection) new CriptoCurrenciesMarketPlaceConnection(connectionParams);
    }

}