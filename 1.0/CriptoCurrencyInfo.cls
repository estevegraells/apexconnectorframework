/**
 * Created by 00070129 on 16/03/2018.
 */

public with sharing class CriptoCurrencyInfo {

    public String id{
        get { return id; }
        set { id = value; }
    }

    public String name{
        get { return name; }
        set { name = value; }
    }

    public String symbol{
        get { return symbol; }
        set { symbol = value; }
    }

    public Integer rank{
        get { return rank; }
        set { rank = value; }
    }

    public Decimal price_usd{
        get { return price_usd; }
        set { price_usd = value; }
    }

    public Decimal percent_change_1h{
        get { return percent_change_1h; }
        set { percent_change_1h = value; }
    }

    public Decimal percent_change_24h{
        get { return percent_change_24h; }
        set { percent_change_24h = value; }
    }

    public Decimal percent_change_7d{
        get { return percent_change_7d; }
        set { percent_change_7d = value; }
    }

    public CriptoCurrencyInfo(String Ident, String n, String s, Integer r, Decimal pusd,
    Decimal pc1h, Decimal pc24h, Decimal pc7d, String lu){

        id = ident;
        name = n;
        symbol = s;
        rank = r;
        price_usd = pusd;
        percent_change_1h = pc1h;
        percent_change_7d = pc7d;
        percent_change_24h = pc24h;
    }

}